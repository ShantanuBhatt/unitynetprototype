﻿
using UnityEngine;
[System.Serializable]
public class PlayerWeapon 
{

    public string name = "SixShooter";
    public int damage = 10;
    public float range = 300f;
}
